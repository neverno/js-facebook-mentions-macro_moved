# facebook-mentions (FacebookMentions)

Library to parse and process Facebook @ + mention macros that are passed on through various Facebook APIs, i.e. @[id:type:text] sequences.

## API

If this was a TypeScript library (which it is not, hmm..) it would look something like:

```javascript
export interface IParsedFacebookMention
{
        macro: string;
        offset: number;
        length: number,
        id: string;
        type: string;
        typeId: number;
        name: string;
        text?: string;
}

export interface IFacebookMessageTag
{
        id: string;
        name: string;
        type: string;
        offset: number;
        length: number,
}

// see: https://developers.facebook.com/docs/graph-api/reference/v2.8/post
export interface IMinimalFacebookPost
{
        message: string;
        message_tags: IFacebookMessageTag[];
}

export class FacebookMentions
{
        readonly types: {[key: string | number]: string | number};
        readonly knownTypeIds: number[];

        static parse(text: string, includeTextInResults: boolean = false): IParsedFacebookMention;

        // parse as a minimal Facebook graph post object - only user|page|group mentions in tags
        static parseAsPost(text: string, includeTextInResults: boolean = false): IMinimalFacebookPost;
}
```

## Example

```javascript
const FacebookMentions = require('@never.no/facebook-mentions');

const text = 'With the Vietnam War ramping up, college students begin to worry about the draft. One of many perspectives in The Vietnam War: An Epic Event from @[125084817502322:274:Ken Burns (PBS)] and @[213113752038097:274:Lynn Novick]. Coming September 2017. More: to.pbs.org';

let mentions = FacebookMentions.parse(text);
let post = FacebookMentions.parseAsPost(text);

console.log("// parse():");
console.dir(mentions);

console.log("\n// parseAsPost():");
console.dir(post);

console.log("\n// cross-check:");
console.dir(post.message_tags.map(t =>
{
    let m = { offset: t.offset, length: t.length, name: t.name, substr: post.message.substr(t.offset, t.length) };
    m.match = m.name == m.substr;
    return m;
}))
```

..should produce something like:

```javascript
// parse():
[ { macro: '@[125084817502322:274:Ken Burns (PBS)]',
    offset: 146,
    length: 38,
    id: '125084817502322',
    type: 'page',
    typeId: 274,
    name: 'Ken Burns (PBS)' },
  { macro: '@[213113752038097:274:Lynn Novick]',
    offset: 189,
    length: 34,
    id: '213113752038097',
    type: 'page',
    typeId: 274,
    name: 'Lynn Novick' } ]

// parseAsPost():
{ message: 'With the Vietnam War ramping up, college students begin to worry about the draft. One of many perspectives in The Vietnam War: An Epic Event from Ken Burns (PBS) and Lynn Novick. Coming September 2017. More: to.pbs.org',
  message_tags:
   [ { id: '125084817502322',
       name: 'Ken Burns (PBS)',
       type: 'page',
       offset: 146,
       length: 15 },
     { id: '213113752038097',
       name: 'Lynn Novick',
       type: 'page',
       offset: 166,
       length: 11 } ] }

// cross-check:
[ { offset: 146,
    length: 15,
    name: 'Ken Burns (PBS)',
    substr: 'Ken Burns (PBS)',
    match: true },
  { offset: 166,
    length: 11,
    name: 'Lynn Novick',
    substr: 'Lynn Novick',
    match: true } ]
```
