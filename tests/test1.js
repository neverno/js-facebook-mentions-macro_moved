var glob = require('glob');
var csv = require('csv');
var FacebookMentions = require('../index');

function readLines(files, done)
{
  let lines = [];
  var next = function()
  {
    let f = files.shift();
    if (f == null) return done(lines);
    console.log("read: " + f);
    let text = fs.readFileSync(f).toString();
    console.log("parse..");
    csv.parse(text, (e, d) =>
    {
      console.log("Done");
      lines = lines.concat(d);
      next();
    });
  }
  next();
}

const fs = require('fs');

readLines(glob.sync("C:\\Users\\rune\\Desktop\\2137\\c\\*.csv"), lines => {
  lines.forEach(l =>
  {
    var mentions = FacebookMentions.parse(l[4] + ' ' + l[9], true);

    //console.dir(mentions);

    var unknowns = mentions.filter(m => m.type == undefined);
    if (unknowns.length > 0) console.log(unknowns.map(u => u.typeId + " " + u.id + " " + u.text).join("\n---\n"));
  });
})
