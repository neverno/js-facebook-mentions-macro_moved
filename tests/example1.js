//const FacebookMentions = require('@never.no/facebook-mentions');
const FacebookMentions = require('../index');

const text = 'With the Vietnam War ramping up, college students begin to worry about the draft. One of many perspectives in The Vietnam War: An Epic Event from @[125084817502322:274:Ken Burns (PBS)] and @[213113752038097:274:Lynn Novick]. Coming September 2017. More: to.pbs.org @[125084817502322:1:Like]';

let mentions = FacebookMentions.parse(text);
let post = FacebookMentions.parseAsPost(text);

console.log("// parse():");
console.dir(mentions);

console.log("\n// parseAsPost():");
console.dir(post);

console.log("\n// cross-check:");
console.dir(post.message_tags.map(t =>
{
    let m = { offset: t.offset, length: t.length, name: t.name, substr: post.message.substr(t.offset, t.length) };
    m.match = m.name == m.substr;
    return m;
}))