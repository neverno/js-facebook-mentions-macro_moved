function FacebookMentions()
{
}

// I'm sure there's more of these - feel free to PR with additional:
// 0 and 1 seem to be from users fooling around typing stuff on their own?
FacebookMentions.types = {

    //0: '0', // error

    // 1 might be a "Like" link..?
    //1: '1', // error

    22: 'photo',

    // 25 376434012481777 @[376434012481777:25:] https://www.facebook.com/permalink.php?id=100014913614890&story_fbid=179172479256538
    // 25 1900652650150318 Add moi bro heavy supporter #Adoor chekkan ? @[1900652650150318:25:] https://www.facebook.com/permalink.php?id=100003589084683&story_fbid=1133638636765762
    25: 'photos_profile',

    69: 'group',

	128: 'app',

	274: 'page',

	844: 'event',

    // 1583 567444179946851 @[567444179946851:1583:] https://www.facebook.com/permalink.php?id=1449107388664458&story_fbid=1779912185583975 --> https://www.facebook.com/help/community/question/?id=567444179946851
    1583: 'community_question',

	2048: 'user',

    // 3654 1750716921860817 @[1750716921860817:3654:] https://www.facebook.com/permalink.php?id=100007675778563&story_fbid=1832092110389964
    //3654: '???',

	4777: 'photos', // "media set",

    // 5512: 'group', // ??? 5512 1116304558427421 @[1116304558427421:5512:] genit ning kanu sejen_- https://www.facebook.com/permalink.php?id=100011261355087&story_fbid=379512079100834

    'photo': 22,

    'photos_profile': 25,

    'group': 69,

    'app': 128,

    'page': 274,

    'event': 844,

    'community_question': 1583,

    'user': 2048,

    'photos': 4777
};

FacebookMentions.knownTypeIds = Object.keys(FacebookMentions.types).filter(function(id) { return /^\d+$/.test(id) }).map(function(id) { return parseInt(id) });

FacebookMentions.parse = function(text, includeText)
{
    if (text == null || typeof text !== 'string') return [];  

    includeText = (includeText === true);
    var mentions = [];
    var re = /\@\[(\d+):(\d+):(.*?)\]/g;
    //var re = new RegExp("\@\[(\d+):(\d+):(.*?)\]", "g");

    var m = re.exec(text);
    while (m != null)
    {
        var mt = {
            macro: m[0],
            offset: m.index,
            length: m[0].length,
            id: m[1],
            type: FacebookMentions.types[m[2]],
            typeId: parseInt(m[2]),
            name: m[3]      
        };
    
        if (includeText) mt.text = text;

        mentions.push(mt);

        // next
        m = re.exec(text);
    }
    return mentions;
}

// static parseAsPost(text: string): IMinimalFacebookPost;
FacebookMentions.parseAsPost = function(text)
{
    var mentions = FacebookMentions.parse(text)

    // replace all mentions and create tags
    var tags = [];
    var delta = 0;
    mentions.forEach(function(m)
    {
        // replace text
        text = text.substr(0, m.offset - delta) + m.name + text.substr(m.offset + m.length - delta);

        // only user|page|group are supported
        if (m.type == 'user' || m.type == 'page' || m.type == 'group')
        {
            tags.push({
                id: m.id,
                name: m.name,
                type: m.type,
                offset: m.offset - delta,
                length: m.name.length
            })
        }

        // adjust delta
        delta += m.length - m.name.length;
    });

    // see: https://developers.facebook.com/docs/graph-api/reference/v2.8/post
    return {
        "message": text,
        "message_tags": tags
    };
}

module.exports = FacebookMentions;
